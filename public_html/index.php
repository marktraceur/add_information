<?php

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');

include_once ( 'php/common.php' ) ;

$language = get_request ( 'language' , 'commons' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;

$project2 = $project ;
if ( $language == 'commons' ) $project2 = 'wikimedia' ;

$image = get_request ( "image" ) ;
$user = get_request ( "user" ) ;
$debug = isset ( $_REQUEST['debug'] ) ;

$db = openDB ( $language , $project ) ;

if ( $image == "" ) {
  print get_common_header ( "add_information.php" , "Add Information" ) ;
  print "<div>This tool can help you adding an {{Information}} template to an image on $language.$project.<br/>" ;
  print "Given an image, it will create a dummy button and then \"click\" on it via JavaScript.<br/>" ;
  print "If you have JavaScript disabled, you'll have to click on the button yourself.<br/>" ;
  print "A pre-filled edit page for the image will open. Please check all alterations, and save the new page.</div>" ;
  print "<br/><form method='get' class='form-inline'>" ;
  print "<input type='hidden' name='language' value='$language' />" ;
  print "<input type='hidden' name='project' value='$project2' />" ;
  print "File : <input name='image' type='text' class='input-xlarge'> <input type='submit' name='doit' value='Do it'></form>" ;
  exit ;
}

$painting = false ;
$mynoinfo = get_request ( 'mynoinfo' , false ) ;
$image_details = Array () ;

function load_image_details () {
	global $image_details , $image , $db ;
	if ( count ( $image_details ) > 0 ) return ;
	$i2 = $image ;
	make_db_safe ( $i2 , true ) ;
	$sql = "SELECT * FROM image WHERE img_name=\"{$i2}\"" ;

	$pages = Array () ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$image_details = $o ;
	}
}


load_image_details () ;
if ( $user == "" ) $user = $image_details->img_user_text ;

$user = str_replace ( "_" , " " , $user ) ;
$title = "Image:" . $image ;
$is_on_toolserver = false ;
$text = get_wikipedia_article ( $language , $title , false , $project2 ) ;

if ( stripos ( $text , "painting" ) !== false ) $painting = true ;

$text = str_replace ( "{{msg:" , "{{" , $text ) ;
$text = str_replace ( "}}[[" , "}}\n[[" , $text ) ;
$text = str_replace ( "]]{{" , "]]\n{{" , $text ) ;

if ( $language == 'de' ) {
	$text = str_ireplace ( '[[Kategorie:' , '[[Category:' , $text ) ;
	$text = str_ireplace ( '[[Benutzer:' , '[[User:' , $text ) ;
	
	$tl = explode ( "\n" , $text ) ;
	foreach ( $tl AS $l => $text ) {
		$text = preg_replace ( '/^\*{0,1}\s*Beschreibung\s*:\s*(.*)$/i' , "== Description ==\n\\1\n== dummy ==\n" , $text ) ;
		$text = preg_replace ( '/^\*{0,1}\s*Quelle\s*:\s*(.*)/i' , "== Source ==\n\\1\n== dummy ==\n" , $text ) ;
		$text = preg_replace ( '/^\*{0,1}\s*Autor\s*:\s*(.*)$/i' , "== Author ==\n\\1\n== dummy ==\n" , $text ) ;
		$text = preg_replace ( '/^\*{0,1}\s*Urheber\s*:\s*(.*)$/i' , "== Author ==\n\\1\n== dummy ==\n" , $text ) ;
		$text = preg_replace ( '/^\*{0,1}\s*Lizenz\w*\s*:\s*(.*)$/i' , "== Permission ==\n\\1\n== dummy ==\n" , $text ) ;
		$text = preg_replace ( '/^\*{0,1}\s*Genehmigung\w*\s*:\s*(.*)$/i' , "== Permission ==\n\\1\n== dummy ==\n" , $text ) ;
		$text = preg_replace ( '/^\*{0,1}\s*Andere Version\w*\s*:\s*(.*)$/i' , "== Other versions ==\n\\1\n== dummy ==\n" , $text ) ;
		$tl[$l] = $text ;
	}
	$text = implode ( "\n" , $tl ) ;
	if ( $debug ) {
		print "<pre>" . htmlspecialchars($text) . "</pre>" ;
	}
}

$text .= " " ;
$t2 = "" ;
$in = 0 ;
for ( $a = 0 ; $a < strlen ( $text ) ; $a++ ) {
  $c = $text[$a] ;
  if ( $c == '.' && $in == 0 && substr ( $text , $a+1 , 1 ) == ' ' ) {
    $c .= "\n" ;
  } else if ( $c == '(' || $c == '[' || $c == '{' ) {
    if ( $in == 0 && $c == "{" ) $t2 .= "\n" ;
    $in++ ;
  } else if ( $c == ')' || $c == ']' || $c == '}' ) {
    $in-- ;
  }
  $t2 .= $c ;
}
$text = $t2 ;
$text = str_replace ( "[[Category:" , "\n[[Category:" , $text ) ;

$lines = explode ( "\n" , $text ) ;
$after = Array() ;
$author = Array() ;
$desc = Array() ;
$source = Array() ;
$date = Array() ;
$permissions = Array() ;
$other_versions = Array() ;
$scanfordate = "" ;
foreach ( $lines AS $l ) {
  $l = trim ( $l ) ;
  if ( $l == "" ) continue ;
  if ( substr ( $l , 0 , 2 ) == "{{" ) {
    if ( $l[4] != '|' ) { $after[] = $l ; continue ; }
    else { $desc[] = "\n$l" ; continue ; }
  }
  if ( stripos ( $l , "[[Category:" ) !== false ) { $after[] = $l ; $scanfordate .= " $l" ; continue ; }
  if ( $mynoinfo && stripos ( $l , "[[User:$user|$user]]" ) !== false ) { $author[] = "~~~" ; $source[] = "own work" ; $scanfordate .= " $l" ; continue ; }
  if ( stripos ( $l , "[[User:" ) !== false ) { $author[] = $l ; $scanfordate .= " $l" ; continue ; }
  if ( stripos ( $l , "{{int:filedesc}}" ) !== false ) { continue ; }
  if ( stripos ( $l , "{{int:license-header}}" ) !== false ) { $after[] = $l ; continue ; }
  if ( substr ( $l , 0 , 1 ) == "*" ) $l = "\n$l" ;
  $desc[] = $l ;
}

// Guess from headings
$lh = '' ;
foreach ( $desc AS $k => $v ) {
	$a = array () ;
	if ( preg_match ( '/^\=+\s*(.+?)\s*\=+\s*$/' , $v , $a ) ) {
		$lh = strtolower ( $a[1] ) ;
	} else {
		if ( $lh == 'permission' ) { $permissions[] = $v ; unset ( $desc[$k] ) ; }
		if ( $lh == 'source' ) { $source[] = $v ; unset ( $desc[$k] ) ; }
		if ( $lh == 'other versions' ) { $other_versions[] = $v ; unset ( $desc[$k] ) ; }
	}
}

// Remove headings in description
foreach ( $desc AS $k => $v ) {
  if ( substr ( $v , 0 , 1 ) == "=" && substr ( $v , -1 , 1 ) == "=" ) unset ( $desc[$k] ) ;
  else if ( strtolower ( substr ( $v , 0 , 6 ) ) == "source" ) {
    $s = trim ( substr ( $v , 6 ) ) ;
    if ( substr ( $s , 0 , 1 ) == ':' ) $s = trim ( substr ( $s , 1 ) ) ;
    else $s = "Source " . $s ;
    $source[] = $s ;
    unset ( $desc[$k] ) ;
  } else if ( strtolower ( substr ( $v , 0 , 4 ) ) == "from" ) {
    $s = trim ( substr ( $v , 4 ) ) ;
    if ( substr ( $s , 0 , 1 ) == ':' ) $s = trim ( substr ( $s , 1 ) ) ;
    $source[] = $s ;
    unset ( $desc[$k] ) ;
  }
}

// If description is empty, use image name
if ( count ( $desc ) == 0 ) {
  $i = explode ( "." , $image ) ;
  array_pop ( $i ) ;
  $i = str_replace ( "_" , " " , implode ( "." , $i ) ) ;
  $i = preg_replace ( '/\s*\(\d\d?\)$/' , '' , $i ) ;
  $i = preg_replace ( '/IMG\s*_*\d*$/' , '' , $i ) ;
  $desc[] = $i ;
}

// Hardcoded source guessing
$ia = implode ( "\n" , $after ) ;
if ( stripos ( $ia , '{{LOC|' ) !== false && count ( $source ) == 0 ) $source[] = "Library of Congress" ;
else if ( stripos ( $ia , '{{LOC}}' ) !== false && count ( $source ) == 0 ) $source[] = "Library of Congress" ;

// Try to extract date from description
$d = implode ( "\n" , $desc ) . " " . $scanfordate ;
$nd = Array () ;
$word = '[äöüÄÜÖßéèáàóòâôû\w]' ;
if ( preg_match_all ( '/\d\d?\. '.$word.'+ \d\d\d\d/' , $d , &$nd , PREG_SET_ORDER ) ) {
  foreach ( $nd AS $dd ) $date[] = $dd[0] ;
} else if ( preg_match_all ( '/'.$word.'+ \d\d?,? \d\d\d\d/' , $d , &$nd , PREG_SET_ORDER ) ) {
  foreach ( $nd AS $dd ) $date[] = $dd[0] ;
} else if ( preg_match_all ( '/\d\d\d\d-\d\d?-\d\d?/' , $d , &$nd , PREG_SET_ORDER ) ) {
  foreach ( $nd AS $dd ) $date[] = $dd[0] ;
} else if ( preg_match_all ( '/'.$word.'{3,15} \d\d\d\d/' , $d , &$nd , PREG_SET_ORDER ) ) {
  foreach ( $nd AS $dd ) $date[] = $dd[0] ;
} else if ( preg_match_all ( '/[^\-xa-zA-Z]\d\d\d\d[^\-a-zA-Z]/' , $d , &$nd , PREG_SET_ORDER ) ) {
  foreach ( $nd AS $dd ) $date[] = $dd[0] ;
}

if ( count ( $date ) == 0 && $language != 'commons' && $language != 'en' && $project != 'wikimedia' ) {
	$a = array () ;
	if ( preg_match ( '/(\d{1,2})\.\s*(\d{1,2})\.\s*(\d{4})\s*/' , $text , $a ) ) {
		$day = $a[1] ;
		$month = $a[2] ;
		$year = $a[3] ;
		if ( strlen ( $day ) == 1 ) $day = "0$day" ;
		if ( strlen ( $month ) == 1 ) $month = "0$month" ;
		$date[] = "$year-$month-$day" ;
	}
}

// Fix lone URLs in source
foreach ( $source AS $k => $v ) {
  $v = trim ( $v ) ;
  if ( substr ( $v , -1 , 1 ) == "." ) $v = trim ( substr ( $v , 0 , strlen ( $v ) - 1 ) ) ;
  if ( substr ( $v , 0 , 1 ) == "[" && substr ( $v , -1 , 1 ) == "]" && strpos ( $v , " " ) === false ) {
    $source[$k] = substr ( $v , 1 , strlen ( $v ) - 2 ) ;
  }
}

// Try to get date from EXIF data
$md = unserialize ( $image_details->img_metadata ) ;
$d = $md["DateTime"] ;
if ( $d == "" ) $d = $md["DateTimeOriginal"] ;
if ( $d == "" ) $d = $md["DateTimeDigitized"] ;
$nd = Array () ;
if ( !$painting && preg_match ( '/\d\d\d\d[\-\.\:]\d\d[\-\.\:]\d\d/' , $d , &$nd ) ) {
  $d = $nd[0] ;
  $d = str_replace ( ":" , "-" , $d ) ;
  $d = str_replace ( "." , "-" , $d ) ;
  if ( count ( $date ) == 1 && trim ( $date[0] ) == $d ) { # Dummy loop
  } else {
    if ( count ( $date ) > 0 ) $d = "; {{according to EXIF data|$d}}" ;
    else $d = "{{according to EXIF data|$d}}" ;
    $date[] = $d ;
  }
}

// Fix ;: in desc
foreach ( $desc AS $k => $v ) {
  if ( substr ( $v , 0 , 1 ) == ";" && strpos ( $v , ":" ) !== false ) $desc[$k] = "\n$v" ;
}

if ( count ( $author ) == 0 && stripos ( $text , "{{self" ) !== false ) {
  $author[] = "[[User:$user|$user]]" ;
  if ( count ( $source ) == 0 ) $source[] = "Self-published work by [[User:$user|$user]]" ;
}


//if ( count ( $source ) == 0 ) $source = $author ; // Relict from MyNoInfo

// Try finding source
foreach ( $desc AS $k => $d ) {
	$nd = Array () ;
	
	if ( count ( $source ) == 0 && preg_match ( '/http:\/\/[a-zA_Z0-9\-_\.\/]+/' , $d , &$nd ) ) {
	  $d = $nd[0] ;
	  $source[] = $d ;
	  unset ( $desc[$k] ) ;
	  continue ;
	}
	if ( count ( $source ) == 0 && preg_match ( '/www\.[a-zA_Z0-9\-_\.\/]+/' , $d , &$nd ) ) {
	  $d = 'http://' . $nd[0] ;
	  $source[] = $d ;
	  unset ( $desc[$k] ) ;
	  continue ;
	}
}    


// Painting mode
if ( $painting ) {
  $ptitle = "" ;
  $author_parts = Array () ;
  foreach ( $desc AS $k => $d ) {
    $nd = Array () ;
    
    if ( count ( $author ) == 0 && preg_match ( '/\bby \[\[[a-zA-Z0-9\- _]+\]\]/' , $d , &$nd ) ) {
      $d = substr ( $nd[0] , 3 ) ;
      $d = str_replace ( "_" , " " , $d ) ;
      $author[] = $d ;
      unset ( $desc[$k] ) ;
      $d = str_replace ( "[" , "" , $d ) ;
      $d = str_replace ( "]" , "" , $d ) ;
      $author_parts = explode ( " " , $d ) ;
      continue ;
    }
    
  }
  
  if ( count ( $author_parts ) > 0 ) {
    $d = str_replace ( "_" , " " , $title ) ;
    $d = explode ( ":" , $d , 2 ) ;
    $d = array_pop ( $d ) ;
    $d = explode ( "." , $d ) ;
    array_pop ( $d ) ;
    $d = implode ( "." , $d ) ;
    foreach ( $author_parts AS $ap ) {
      $d = str_ireplace ( $ap , "" , $d ) ;
    }
    $nd = Array () ;
    if ( count ( $date ) == 0 && preg_match ( '/\d\d\d\d/' , $d , &$nd ) ) {
      $d2 = $nd[0] ;
      $d = str_replace ( $d2 , "" , $d ) ;
      $date[] = $d2 ;
    }
    $d = str_replace ( "  " , " " , $d ) ;
    $d = str_replace ( " IBI" , "" , $d ) ; # Brute force hack
    $ptitle = ucfirst ( trim ( $d ) ) ;
  }
}


if ( count ( $desc ) == 0 ) {
	$a = array () ;
	if ( preg_match ( '/^(.+)\.[a-z]+$/' , $image , $a ) ) $desc[] = $a[1] ;
}


// Create new text
$sep = " " ;

if ( $painting ) {
  $tn = "Painting" ;
  $text = "=={{int:filedesc}}==\n{{Painting
|Title = $ptitle
|Artist = " . implode ( $sep , $author ) . "
|Year = " . implode ( $sep , $date ) . "
|Technique = 
|Dimensions = 
|Gallery = 
|Location = 
|Notes = " . implode ( $sep , $desc ) . "
|Source = " . implode ( " ; " , $source ) . "
|Permission = 
|Other versions = 
}}

" . implode ( "\n" , $after ) ;
} else {
  $tn = "Information" ;
  $text = "=={{int:filedesc}}==\n{{Information
|Description = " . implode ( $sep , $desc ) . "
|Source      = " . implode ( " ; " , $source ) . "
|Date        = " . implode ( $sep , $date ) . "
|Author      = " . implode ( $sep , $author ) . "
|Permission  = " . implode ( $sep , $permissions ) . "
|other_versions = " . implode ( $sep , $other_versions ) . "
}}

" . implode ( "\n" , $after ) ;
}

// Fix lang
if ( $language == 'de' ) {
	$text = str_ireplace ( '[[Category:' , '[[User:' , $text ) ;
	$text = str_ireplace ( '[[User:' , '[[Benutzer:' , $text ) ;

	$text = str_replace ( '|Description' , '|Beschreibung' , $text ) ;
	$text = str_replace ( '|Source' , '|Quelle' , $text ) ;
	$text = str_replace ( '|Date' , '|Datum' , $text ) ;
	$text = str_replace ( '|Author' , '|Urheber' , $text ) ;
	$text = str_replace ( '|Permission' , '|Genehmigung' , $text ) ;
	$text = str_replace ( '|other_versions' , '|Andere Versionen' , $text ) ;
}

if ( $debug ) {
	print "<hr/><pre>" . htmlspecialchars($text) . "</pre>" ;
	exit ;
}


// Create form
$button = cGetEditButton ( $text , $title , $language , $project2 , "Adding {{{$tn}}} template (using http://tools.wmflabs.org/add-information )" , "Click me if you have JavaScript disabled" , false , false , true , true ) ;
$button = str_replace ( "type='submit'" , "type='submit' id='thebutton'" , $button ) ;

// Output
print "<html><head></head>" ;
print "<body onload=\"document.getElementById('thebutton').click();\">" ;
print $button ;
print "<br/>(otherwise, wait a second or two...)" ;
print "</body></html>" ;

?>

