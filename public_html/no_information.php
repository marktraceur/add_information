<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

include_once ( 'php/common.php' ) ;

$templates = array (
  'Information',
  'Painting',
  'Blason-fr-en',
  'Blason-fr-en-it',
  'Blason-xx',
  'COAInformation',
  'Artwork',
  'Art_Photo',
  'Photograph',
  'Book',
  'Map',
  'Musical_work',
  'Specimen'
) ;

function get_no_info ( $offset , $max ) {
	global $db , $templates , $user , $startswith ;
	$namespace = 6 ;
	
	make_db_safe ( $user ) ;
	make_db_safe ( $offset ) ;
	
	$t = implode ( "','" , $templates ) ;

	$ret = array () ;
	$sql = "SELECT page_title FROM page,image WHERE page_namespace=$namespace AND page_title=img_name AND page_id NOT IN ( SELECT tl_from FROM templatelinks WHERE tl_title IN ('$t') )" ;
	if ( $startswith != '' ) $sql .= " AND page_title>='" . get_db_safe ( $startswith ) . "'" ;
	if ( $user != '' ) $sql .= " AND img_user_text='" . str_replace ( '_' , ' ' , get_db_safe ( $user ) ) . "'" ;
	$sql .= " ORDER BY page_title LIMIT $max OFFSET $offset" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[] = $o->page_title ;
	}
	return $ret ;
}


$language = get_request ( 'language' , 'commons' ) ;
$project = get_request ( 'project' , 'wikimedia' ) ;
$offset = get_request ( 'offset' , '0' ) ;
$max = get_request ( 'max' , '100' ) ;
$user = trim ( get_request ( 'user' , '' ) ) ;
$startswith = trim ( get_request ( 'startswith' , '' ) ) ;
$testing = isset ( $_REQUEST['testing'] ) ;

$db = openDB ( $language , $project ) ;


print get_common_header ( '' , 'No {{Information}}' ) ;

print "<p>Get files with no <i>{{Information}}</i> (or similar) template, optionally filtered by user.</p>" ;

print "<div>" ;
print "<form method='get' class='form-inline'>" ;
print "<table class='table table-condensed'><tbody>" ;
print "<tr><th>Wiki</th><td><input name='language' value='$language' type='text' class='span2'/> . <input name='project' value='$project' type='text' class='span2'/></td></tr>" ;
print "<tr><th nowrap>Starts with</th><td><input name='startswith' value='$startswith' type='text' class='span3' /> (optional)</td></tr>" ;
print "<tr><th>User</th><td><input name='user' value='$user' type='text' class='span3' /> (optional)</td></tr>" ;
print "<tr><td/><td><input type='submit' value='Do it' class='btn btn-primary' /></td></tr>" ;
print "</tbody></table>" ;
print "</form>" ;
print "</div>" ;

$files = get_no_info ( $offset , $max+1 ) ;

$base_url = "/add-information/no_information.php?language=$language&project=$project&max=$max" ;
if ( $startswith != '' ) $base_url .= "&startswith=" . urlencode ( $startswith ) ;
if ( $user != '' ) $base_url .= "&user=" . urlencode ( $user ) ;

if ( $offset == 0 ) {
	print "Previous $max" ;
} else {
	$p = $offset - $max ;
	print "<a href='$base_url&offset=$p'>Previous $max</a>" ;
}

print " | " ;

if ( count ( $files ) > $max ) {
	$p = $offset + $max ;
	print "<a href='$base_url&offset=$p'>Next $max</a>" ;
	array_pop ( $files ) ; // Don't show last
} else {
	print "Next $max" ;
}

$p = $offset + 1 ;
print "<ol start=$p>" ;
foreach ( $files AS $f ) {
	$f2 = urlencode ( $f ) ;
	print "<li>" ;
	print "<a target='_blank' href=\"http://$language.$project.org/wiki/File:$f2\">File:" . str_replace('_',' ',$f) . "</a>" ;
	print " <small>(<a target='_blank' href=\"/add-information/?language=$language&project=$project&image=$f2&doit=Do+it\">AddInformation</a>)</small>" ;
	print "</li>" ;
}
print "</ol>" ;


print "</body>" ;
print "</html>\n" ;
myflush() ;

?>
